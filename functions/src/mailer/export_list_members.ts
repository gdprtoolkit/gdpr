import {EXPORT_DOMAIN, EXPORT_EMAIL_ADDRESS, EXPORT_MAILGUN_APIKEY, GOOGLE_CLOUD_PROJECT_ID} from "../config";

const Mailchimp = require('mailchimp-api-v3');
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const PubSub = require('@google-cloud/pubsub');

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: GOOGLE_CLOUD_PROJECT_ID,
});

const mailgun = require('mailgun-js')({apiKey: EXPORT_MAILGUN_APIKEY, domain: EXPORT_DOMAIN});

const json2csv = require('json2csv').parse;
const fields = [
  'email_address',
  'status',
  'merge_fields.FNAME',
  'merge_fields.LNAME',
  'merge_fields.ADDRESS.addr1',
  'merge_fields.ADDRESS.addr2',
  'merge_fields.ADDRESS.city',
  'merge_fields.ADDRESS.state',
  'merge_fields.ADDRESS.zip',
  'merge_fields.ADDRESS.country',
  'merge_fields.PHONE',
  'member_rating',
];
const opts = {fields};
const importJobTopic = 'list-import-job';
const exportJobTopic = 'list-export-job';


async function publishMessage(name, data) {
  // [START pubsub_publish_message]
  // Imports the Google Cloud client library

  // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
  let message = JSON.stringify(data);
  const dataBuffer = Buffer.from(message);

  await pubsubClient
    .topic(name)
    .publisher()
    .publish(dataBuffer)
    .then(results => {
      const messageId = results[0];
      console.log(`Message ${messageId} published: ${message}`);
    })
    .catch(err => {
      console.error('ERROR:', err);
    });
  // [END pubsub_publish_message]
}


export const ExportListMembers = functions.https.onRequest(async (req, res) => {


  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Headers", "*");

  if (req.method == "OPTIONS") {
    res.status(200).send("");
    return;
  }
  const listId = req.query.listId || false;

  if (!listId) {
    req.status(400).send("invalid request");
    return
  }

  const token = req.headers.authorization.split(" ")[1];
  const decodedToken = await admin.auth().verifyIdToken(token);

  const uid = decodedToken.user_id;

  const user = await admin.auth().getUser(uid);
  const userProfile = await admin.database().ref(`/mailchimpUserProject/${uid}`).once("value");
  const userProfileValue = userProfile.val();
  console.log("obtained user", userProfileValue);
  const userProjectId = userProfileValue.projectId;

  console.log("project id", userProjectId);

  // const mailchimpListMembers = await admin.database().ref(`/mailchimpListMembersExport/${uid}/${listId}`).once("value");
  const mailchimpToken = await admin.database().ref(`/mailAccessToken/${uid}`);
  const mailchimpApiData = await admin.database().ref(`/mailchimpApiData/${uid}`);
  const snapshot = await mailchimpToken.once("value");
  const mailchimpApiDataSnapshot = await mailchimpApiData.once("value");
  console.log(snapshot.val());
  const apiData = mailchimpApiDataSnapshot.val();
  const mailchimpTokenValue = snapshot.val();
  const mailchimp = new Mailchimp(mailchimpTokenValue.token.access_token + "-" + apiData.dc);
  const listToExport = await mailchimp.get('/lists/' + listId);
  // console.log("list to export ", listToExport);
  const memberCount = listToExport.stats.member_count;

  const bucketSize = 200;

  let bucketCount = parseInt("" + (memberCount / bucketSize));
  const remainder = memberCount - (bucketSize * bucketCount);
  if (remainder > 0) {
    bucketCount += 1;
  }
  console.log("Bucket count ", bucketCount, " with size of each bucket ", bucketSize, " for list of size", memberCount);
  await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/export`).set("pending");

  for (let i = 0; i < bucketCount; i++) {
    await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/${i}/done`).set(false);
    await publishMessage(importJobTopic, {
      userId: uid,
      listId: listId,
      bucketNumber: i,
      bucketSize: bucketSize,
      bucketCount: bucketCount,
    })
  }
  res.jsonp({
    status: "ok",

    message: "Sync initiated, this make take some time."
  });

});

export const mailingListExporter = functions.pubsub.topic(exportJobTopic).onPublish(async (pubSubMessage) => {
  const data = pubSubMessage.data.json;
  console.log("Initiated mailing list exporter", data);

  const uid = data.userId;

  const user = await admin.auth().getUser(uid);
  const userProfile = await admin.database().ref(`/mailchimpUserProject/${uid}`).once("value");
  const userProfileValue = userProfile.val();
  console.log("obtained user", userProfileValue);
  const userProjectId = userProfileValue.projectId;


  const listId = data.listId;
  console.log("Check if all buckets imported for list ", listId);
  const bucketCount = data.bucketCount;

  const exportStatus = await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/export`).once("value");
  if (exportStatus.val() != "pending") {
    return
  }

  for (let i = 0; i < bucketCount; i++) {
    const listDone = await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/${i}/done`).once("value");
    if (!listDone.val()) {
      console.log("Bucket ", i, " is still not imported");
      return;
    }
  }

  const exportStatusRecheck = await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/export`).once("value");
  if (exportStatus.val() != "pending") {
    return
  }
  await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/export`).set("completed");

  console.log("all buckets imported, export as csv to email");


  let items = [];
  for (let i = 0; i < bucketCount; i++) {
    console.log("Read bucket [", i, "] for exporting mailing list ", listId);
    const listData = await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/${i}/list`).once("value");
    items = items.concat(listData.val().members);
  }


  // console.log("list to email", items);
  try {
    const csv = json2csv(items, opts);
    const attch = new mailgun.Attachment({
      data: Buffer.from(csv, 'utf8'),
      filename: `mailchimp-${listId}.csv`,
      knownLength: csv.length,
      contentType: 'text/csv'
    });

    const mailToSend = {
      from: 'Test <no-reply@mail.authme.io>',
      to: EXPORT_EMAIL_ADDRESS,
      subject: `Subscribers export from mailchimp list ${listId}`,
      text: "Please find the CSV attached.",
      attachment: attch
    };


    mailgun.messages().send(mailToSend, function (error, body) {
      console.log(body);
    });

  } catch (err) {
    console.log("Failed to email mailing list ", listId);
    console.error(err);
  }

});

export const mailingListImporter = functions.pubsub.topic(importJobTopic).onPublish(async (pubSubMessage) => {


  const data = pubSubMessage.data.json;
  console.log("import mailing list part", data);
  try {


    const uid = data.userId;

    const user = await admin.auth().getUser(uid);
    const userProfile = await admin.database().ref(`/mailchimpUserProject/${uid}`).once("value");
    const userProfileValue = userProfile.val();
    console.log("obtained user", userProfileValue);
    const userProjectId = userProfileValue.projectId;


    const listId = data.listId;
    const bucketNumber = data.bucketNumber;
    const bucketSize = data.bucketSize;

    const mailchimpToken = await admin.database().ref(`/mailAccessToken/${uid}`);
    const mailchimpApiData = await admin.database().ref(`/mailchimpApiData/${uid}`);

    const snapshot = await mailchimpToken.once("value");
    const mailchimpApiDataSnapshot = await mailchimpApiData.once("value");
    const apiData = mailchimpApiDataSnapshot.val();
    const mailchimpTokenValue = snapshot.val();
    const mailchimp = new Mailchimp(mailchimpTokenValue.token.access_token + "-" + apiData.dc);
    const lists = await mailchimp.get('/lists/' + listId + "/members?offset=" + bucketNumber * bucketSize + "&count=" + bucketSize);
    // console.log("bucket ", bucketNumber, " response ", lists);
    await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/${bucketNumber}/list`).set(lists);
    await admin.database().ref(`/mailchimpListMembers/${userProjectId}/${listId}/${bucketNumber}/done`).set(true);

    console.log("completed importing bucket", bucketNumber, " with ", lists.length);
    await publishMessage(exportJobTopic, data);
  } catch (e) {
    console.log("failed to get bucket ", data);
    console.log("publish message again");
    await publishMessage(importJobTopic, data);
  }


});