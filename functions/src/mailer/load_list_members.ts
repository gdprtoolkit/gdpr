import {GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT, GOOGLE_CLOUD_PROJECT_ID} from "../config";

const Mailchimp = require('mailchimp-api-v3');
const functions = require('firebase-functions');


const admin = require('firebase-admin');

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
  });

}

export const GetListMembers = functions.https.onRequest(async (req, res) => {


  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Headers", "*");

  if (req.method == "OPTIONS") {
    res.status(200).send("");
    return;
  }

  const force = req.query.force || false;
  const listId = req.query.listId || false;

  if (!listId) {
    req.status(400).send("invalid request");
    return
  }

  const token = req.headers.authorization.split(" ")[1];
  const decodedToken = await admin.auth().verifyIdToken(token);

  const uid = decodedToken.user_id;

  const user = await admin.auth().getUser(uid);
  const userProfile = await admin.database().ref(`/mailchimpUserProject/${uid}`).once("value");
  const userProfileValue = userProfile.val();
  console.log("obtained user", userProfileValue);
  const projectId = userProfileValue.projectId;

  console.log("project id", uid);

  const mailchimpToken = await admin.database().ref(`/mailAccessToken/${uid}`);
  const mailchimpApiData = await admin.database().ref(`/mailchimpApiData/${uid}`);
  const mailchimpListMembers = await admin.database().ref(`/mailchimpListMembers/${projectId}/${listId}`).once("value");

  const cachedLists = mailchimpListMembers.val();


  if ((cachedLists && cachedLists.total_items && cachedLists.list_id) && !force) {
    res.jsonp(cachedLists);

    return
  } else {
    const snapshot = await mailchimpToken.once("value");
    const mailchimpapiDataSnapshot = await mailchimpApiData.once("value");
    console.log(snapshot.val());
    const apiData = mailchimpapiDataSnapshot.val();
    const mailchimpTokenValue = snapshot.val();
    const mailchimp = new Mailchimp(mailchimpTokenValue.token.access_token + "-" + apiData.dc);
    const lists = await mailchimp.get('/lists/' + listId + "/members");

    await admin.database().ref(`/mailchimpListMembers/${projectId}/${listId}`).set(lists);
    res.jsonp(lists);
    return

  }


});