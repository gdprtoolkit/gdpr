import {GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT} from "../config";

const Mailchimp = require('mailchimp-api-v3');
const functions = require('firebase-functions');


const admin = require('firebase-admin');
if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
  });

}

export const GetMailingLists = functions.https.onRequest(async (req, res) => {
  try {


    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Headers", "*");

    if (req.method == "OPTIONS") {
      res.status(200).send("");
      return;
    }

    const force = req.query.force || false;
    if (!req.headers.authorization) {
      res.status(403).send("");
      return
    }


    const token = req.headers.authorization.split(" ")[1];
    console.log("token to verify: ", token);

    const decodedToken = await admin.auth().verifyIdToken(token);
    console.log("decoded token", decodedToken);

    const uid = decodedToken.user_id;

    const user = await admin.auth().getUser(uid);
    const userProfile = await admin.database().ref(`/mailchimpUserProject/${uid}`).once("value");
    const userProfileValue = userProfile.val();
    console.log("obtained user", userProfileValue);
    const projectId = userProfileValue.projectId;

    console.log("project id", projectId);

    const mailchimpToken = await admin.database().ref(`/mailAccessToken/${uid}`);
    const mailchimpApiData = await admin.database().ref(`/mailchimpApiData/${uid}`);
    const mailchimpLists = await admin.database().ref(`/mailchimpLists/${projectId}`).once("value");

    const existingLists = mailchimpLists.val();


    if ((existingLists && existingLists.statusCode && existingLists.statusCode == 200) && !force) {
      console.log("serve from cache")
      for (let i = 0; i < existingLists.lists.length; i++) {
        const listId = existingLists.lists[i].id;
        const status = await admin.database().ref(`/mailchimpListMembers/${projectId}/${listId}/export`).once('value');
        console.log("load stats for list", listId, status.val());
        if (!status || !status.val()) {
          existingLists.lists[i].gdpr_status = "unmanaged";
        } else {
          existingLists.lists[i].gdpr_status = status.val();
        }
      }

      res.jsonp(existingLists);

    } else {

      console.log("query mailchimp for lists", existingLists);

      const existingListsMap = {};

      if (existingLists && existingLists.lists) {
        for (let i = 0; i < existingLists.lists.length; i++) {
          existingListsMap[existingLists.lists[i].id] = existingLists.lists[i]
        }
      }


      const snapshot = await mailchimpToken.once("value");
      const mailchimpapiDataSnapshot = await mailchimpApiData.once("value");
      const apiData = mailchimpapiDataSnapshot.val();

      const mailchompTokenValue = snapshot.val();

      const mailchimp = new Mailchimp(mailchompTokenValue.token.access_token + "-" + apiData.dc);
      const lists = await mailchimp.get('/lists');

      for (let i = 0; i < lists.lists.length; i++) {
        const listId = lists.lists[i].id;
        const existingList = existingListsMap[listId];
        if (existingList) {
          lists.lists[i].gdpr_status = existingList.gdpr_status;
        } else {
          lists.lists[i].gdpr_status = "unmanaged";
        }
      }


      await admin.database().ref(`/mailchimpLists/${projectId}`).set(lists);

      res.jsonp(lists);

    }
  } catch (e) {
    console.log("Failed to load mailing lists", e);
    res.status(500).send("");

  }


});