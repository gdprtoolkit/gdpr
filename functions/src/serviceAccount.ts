import {GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT} from "./config";

const admin = require('firebase-admin');

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
  });

}