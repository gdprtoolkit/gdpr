/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


'use strict';

const functions = require('firebase-functions');

// Firebase Setup

import {authorize} from './oauth/authorize'
import {newToken} from './oauth/newToken'
import {handleLogout} from './oauth/logout'
import {GetMailingLists} from './mailer/load_mailing_lists'
import {GetListMembers} from './mailer/load_list_members'
import {ExportListMembers, mailingListExporter, mailingListImporter} from "./mailer/export_list_members";

exports.hello = functions.https.onRequest((request, response) => {
  response.send("Hello from gdpr!");
});


/**
 * authorize the User to the mailchimp authentication consent screen. Also the 'state' cookie is set for later state
 * verification.
 */
exports.authorize = authorize;

/**
 * Logout sets the token cookie to null and expiry time to 0 to remove the token cookie
 */

exports.logout = handleLogout;
/**
 * Exchanges a given Mailchimp auth code passed in the 'code' URL query parameter for a Firebase auth token.
 * The request also needs to specify a 'state' query parameter which will be checked against the 'state' cookie.
 * The Firebase custom auth token, display name, photo URL and Mailchimp acces token are sent back in a JSONP callback
 * function with function name defined by the 'callback' query parameter.
 */
exports.token = newToken;


exports.getLists = GetMailingLists;
exports.getListMembers = GetListMembers;
exports.exportListMembers = ExportListMembers;

exports.mailingListImporter = mailingListImporter;
exports.mailingListExporter = mailingListExporter;