import {GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT} from "./config";

const firebase = require('firebase');

const firebaseAdmin = require('firebase-admin');
const serviceAccount = GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT;


const firebaseApp = firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
});

export default firebaseApp