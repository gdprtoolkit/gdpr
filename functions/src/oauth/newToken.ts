import {mailchimpOAuth2Client} from "./client";
import {FRONTEND_URL, OAUTH_REDIRECT_URI, OAUTH_SCOPES} from "./const";
import axios from "axios";
import {GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT} from "../config";

const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');
// import "@firebase/firestore";


const admin = require('firebase-admin');


admin.initializeApp({
  credential: admin.credential.cert(GOOGLE_CLOUD_CLIENT_SERVICE_ACCOUNT),
  databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
});


export const newToken = functions.https.onRequest((req, res) => {
  const oauth2 = mailchimpOAuth2Client();

  return cookieParser()(req, res, async () => {
    try {

      if (req.query.error) {
        throw req.query.error;
      }

      // if (!req.cookies.state) {
      //   throw new Error('State cookie not set or expired. Maybe you took too long to authorize. Please try again.');
      // } else if (req.cookies.state !== req.query.state) {
      //   throw new Error('State validation failed');
      // }

      // Get the access token object (the authorization code is given from the previous step).
      const tokenConfig = {
        code: req.query.code,
        redirect_uri: OAUTH_REDIRECT_URI,
        scope: OAUTH_SCOPES,
        client_id: '224569933023',
        client_secret: '78a740da5c4a7d88ce6b7a377c617dc87f0117b9e56ca1d422',
      };


      const result = await oauth2.authorizationCode.getToken(tokenConfig);
      const accessToken = oauth2.accessToken.create(result);

      const response = await axios({
        url: "https://login.mailchimp.com/oauth2/metadata",
        headers: {
          "Authorization": "OAuth " + result.access_token
        }
      });

      console.log("response from mailchimp", response.data);
      const projectId = response.data.user_id;
      const firebaseAccount = await createFirebaseAccount(response.data.login.login_email, response.data.login.login_name, response.data.login.login_id, accessToken, response.data);
      const uid = `mailchimp:${response.data.login.login_email.replace(/[\.@]/g, '')}`;
      await admin.database().ref(`/mailchimpUserProject/${uid}`).set({
        projectId: projectId
      });


      res.cookie('token', firebaseAccount, {
        maxAge: 3600000,
        secure: false,
        httpOnly: false,
      });

      res.redirect(FRONTEND_URL + "/" + "?token=" + firebaseAccount);

      // // We have an Mailchimp access token and the user identity now.
      // const accessToken = results.access_token;
      // const mailchimpUserID = results.user.id;
      // const profilePic = results.user.profile_picture;
      // const userName = results.user.full_name;
      //
      // // Create a Firebase account and get the Custom Auth Token.
    } catch (error) {
      console.log("Failed to generate token", error);
      // return res.jsonp({
      //   error: error.toString,
      // });
      res.redirect(FRONTEND_URL + "/" + "?error=" + error);
    }
  });

});

/**
 * Creates a Firebase account with the given user profile and returns a custom auth token allowing
 * signing-in this account.
 * Also saves the accessToken to the datastore at /mailchimpAccessToken/$uid
 *
 * @returns {Promise<string>} The Firebase custom auth token in a promise.
 */
function createFirebaseAccount(email, displayName, mailChimpUserId, accessToken, mailchimpApiData) {
  // The UID we'll assign to the user.
  const uid = "mailchimp:" + email.replace(/[\.@]/g, '');

  // Save the access token to the Firebase Realtime Database.
  const databaseTask = admin.database().ref(`/mailAccessToken/${uid}`)
    .set(accessToken);

  // Save the access token to the Firebase Realtime Database.
  const apiDataTask = admin.database().ref(`/mailchimpApiData/${uid}`)
    .set(mailchimpApiData);

  // Create or update the user account.
  const userCreationTask = admin.auth().updateUser(uid, {
    displayName: displayName,
    email: email,
    mailChimpUserId: mailChimpUserId,
  }).catch((error) => {
    console.log("failed to update existing user by uid", uid, email, error);
    // If user does not exists we create it.
    if (error.code === 'auth/user-not-found') {
      return admin.auth().createUser({
        uid: uid,
        displayName: displayName,
        email: email,
        mailChimpUserId: mailChimpUserId,
      });
    }
    throw error;
  });

  // Wait for all async task to complete then generate and return a custom auth token.
  return Promise.all([userCreationTask, databaseTask, apiDataTask]).then(() => {
    // Create a Firebase custom auth token.
    return admin.auth().createCustomToken(uid);
  }).then((token) => {
    return token;
  });
}