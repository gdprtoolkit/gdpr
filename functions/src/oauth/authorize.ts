import {mailchimpOAuth2Client} from "./client";
import {OAUTH_REDIRECT_URI, OAUTH_SCOPES} from "./const";

const crypto = require('crypto');

const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');


export const authorize = functions.https.onRequest((req, res) => {
  const oauth2 = mailchimpOAuth2Client();

  cookieParser()(req, res, () => {
    try {
      const state = crypto.randomBytes(20).toString('hex');
      res.cookie('state', state.toString(), {
        maxAge: 3600000,
        secure: false,
        httpOnly: true,
      });
      console.log("Redirect uri: ", OAUTH_REDIRECT_URI);
      const redirectUri = oauth2.authorizationCode.authorizeURL({
        redirect_uri: OAUTH_REDIRECT_URI,
        scope: OAUTH_SCOPES,
        state: state,
      });
      res.redirect(redirectUri);

    } catch (e) {
      console.log("failed to sent to authorize", e);
      res.redirect("/")
    }
  });
})