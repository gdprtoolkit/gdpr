import { FRONTEND_URL } from "./const";


const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');


export const handleLogout = functions.https.onRequest((req, res) => {

  cookieParser()(req, res, () => {
    const token = req.cookies.token;
    res.cookie('token', null, {
      maxAge: 1,
      secure: false,
      httpOnly: true,
    });

    res.redirect(FRONTEND_URL);
  });
})