// const functions = require('firebase-functions');

/**
 * Creates a configured simple-oauth2 client for mailchimp.
 */
export function mailchimpOAuth2Client() {
  // mailchimp OAuth 2 setup
  // TODO: Configure the `mailchimp.client_id` and `mailchimp.client_secret` Google Cloud environment variables.
  const credentials = {
    client: {
      id: '224569933023',
      secret: '78a740da5c4a7d88ce6b7a377c617dc87f0117b9e56ca1d422',
    },
    auth: {
      tokenHost: 'https://login.mailchimp.com',
      tokenPath: '/oauth2/token',
      authorizePath: '/oauth2/authorize',
    },
  };
  return require('simple-oauth2').create(credentials);
}
