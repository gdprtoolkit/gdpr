"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("./client");
const const_1 = require("./const");
const crypto = require('crypto');
const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');
exports.authorize = functions.https.onRequest((req, res) => {
    const oauth2 = client_1.mailchimpOAuth2Client();
    cookieParser()(req, res, () => {
        try {
            const state = crypto.randomBytes(20).toString('hex');
            res.cookie('state', state.toString(), {
                maxAge: 3600000,
                secure: false,
                httpOnly: true,
            });
            console.log("Redirect uri: ", const_1.OAUTH_REDIRECT_URI);
            const redirectUri = oauth2.authorizationCode.authorizeURL({
                redirect_uri: const_1.OAUTH_REDIRECT_URI,
                scope: const_1.OAUTH_SCOPES,
                state: state,
            });
            res.redirect(redirectUri);
        }
        catch (e) {
            console.log("failed to sent to authorize", e);
            res.redirect("/");
        }
    });
});
//# sourceMappingURL=authorize.js.map