"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("./client");
const const_1 = require("./const");
const axios_1 = require("axios");
const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');
// import "@firebase/firestore";
const admin = require('firebase-admin');
const serviceAccount = {
    "type": "service_account",
    "project_id": "gdpr-toolkit",
    "private_key_id": "1d877b3e4c5b2a46f327c6919a5479cf6102a2fe",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDW4/1rN7sprXdJ\nMbNe2gqYXCndN1nId1C662qvp7tLfsVd7jO4SGxWs+8dLU8NjuMw7UdG6567bRWO\nEWlWRndc7GFyvFOMLHbn2OZkO9gc4xbg1UOH23i4g250rGynR909xDlCLhQHTfGs\nUTcH0KQo0xDBWyxiSi3ANjXsXL9c9HqHhcCybC4YDTFo5YCd49QqfnFnEdEvlIeP\n6YQWnZ64Vy5Oq6M02Rcq/K7thiInauypfytq720J6A6Bp0UteUTjJOq09cKZvREb\nMGkQIcNBB4zhUe9JaBqW6Eh2Nh9xAv+oT0F0VkxgBGydY95wsQWHCHmIw4DI12Qi\nZ9+t2j8bAgMBAAECggEAAKyjJ/Sl0WH/pNV+GEcbj2eCo3TAAwwKJHwGpVnOZv8/\n2U/yY5qpPwsTg1uYCuqENgFt9CqwddVyzYj/yUsxUV5V3p6kAyWZ6Jev/r0yrBcz\nrthHJ6RTAv6OmIPOAicMlQa080s3uYokvwNbkS+2GYFdZRicfAvQNvE0nB2dEB/x\neoa82bamB5uIVx8yanOyjENx34ICMsTSb4mnJt6H7Pg9Ed4Z9QlGZFAKqATCrhXo\n5QxvgdmUpJbechUzs58+SzcGW7Loh6ynu5aqKDinOAQ4dqCZVP3ONpP7uXHeXgnN\nLIk9t1NMXl2XxA1VaiKhbDEY3SVSBpSwMvrU1A3EKQKBgQD2J7Ae6e7KeMwdt2Bz\nLfR8RBly7EfeVzg89gij/P3fOlMUz3uQg5RXIUDE8IBQ0QiXgpn1Lqenuxqf297I\nMm8VbNvh/a/q9w0Tv+BzDAbM4kkYJ8B68twziHOjfHQTyvQdlwTHDwQi5VQzKmVv\nh3qXPzL3a8CsgiVl6tpk9u0QpQKBgQDffDGwKIr8Z8k7ikOoOJxcgz1oV0D6lzG3\nRl761Y79739Ha3lQwf2utcwiV16Rof5dFk1+ExhFt/e1qEveSfD7BxuCoPNntsmf\nSI/CS0qLUb/u5eLT3M5y0aJStI8+zG9LCOQYmf8lVWT2Qr2Kio6G+0IlSS1aABn+\noVEkJCNEvwKBgAq+7sxasAb8wQRnFfnRPsQSAP5oXDbTnevjXbsy9y5AkJHvANjt\nNZOcKwSt7xwYFATxOaFfeQg0fdQOuCoLMZhA2VSDS6O5R0Pec4DyPHXDRyebzKZR\nZdWGFm5Dp5mZmZCVICtEd4zCcCuPpT/6LRrfMm1ksdOWFuXdG82ejfjVAoGAHDme\nOOZV71zYQGnM87D6poB4v/XNIhj4UXJUyo8wOQgHZii/sKBffrI9LtUj/3abHnyT\nmumkdiGu6kVwF4n1Im8aXVwggafHPRO3/EzwBDp16CEmjaVkeYHaY2GkecTuBWcB\nD85bOAbZ8nQpZhbru0Z+ayEb1jMPFWagTdU2kxcCgYATHz4hOcGWakHeLv5xszwI\nW+jHwmhReCvw8DWgWwoTAqtpQ476qjeeS2DWq84ustX3ofYQq5e12ur9e7Y+tja3\nPSBbf1YBgh9UbEW3LIcq0HK671j8Ec0oT6RzH+ipCiWCSlVCN1ZEQOFhCWx/VJ0a\nK+vzOOB73KBlFEI6fb9iIw==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-kw3by@gdpr-toolkit.iam.gserviceaccount.com",
    "client_id": "101642364256342826296",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-kw3by%40gdpr-toolkit.iam.gserviceaccount.com"
};
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
});
exports.newToken = functions.https.onRequest((req, res) => {
    const oauth2 = client_1.mailchimpOAuth2Client();
    return cookieParser()(req, res, () => __awaiter(this, void 0, void 0, function* () {
        try {
            if (req.query.error) {
                throw req.query.error;
            }
            // if (!req.cookies.state) {
            //   throw new Error('State cookie not set or expired. Maybe you took too long to authorize. Please try again.');
            // } else if (req.cookies.state !== req.query.state) {
            //   throw new Error('State validation failed');
            // }
            // Get the access token object (the authorization code is given from the previous step).
            const tokenConfig = {
                code: req.query.code,
                redirect_uri: const_1.OAUTH_REDIRECT_URI,
                scope: const_1.OAUTH_SCOPES,
                client_id: '224569933023',
                client_secret: '78a740da5c4a7d88ce6b7a377c617dc87f0117b9e56ca1d422',
            };
            const result = yield oauth2.authorizationCode.getToken(tokenConfig);
            const accessToken = oauth2.accessToken.create(result);
            const response = yield axios_1.default({
                url: "https://login.mailchimp.com/oauth2/metadata",
                headers: {
                    "Authorization": "OAuth " + result.access_token
                }
            });
            console.log("response from mailchimp", response.data);
            const projectId = response.data.user_id;
            const firebaseAccount = yield createFirebaseAccount(response.data.login.login_email, response.data.login.login_name, response.data.login.login_id, accessToken, response.data);
            const uid = `mailchimp:${response.data.login.login_email.replace(/[\.@]/g, '')}`;
            yield admin.database().ref(`/mailchimpUserProject/${uid}`).set({
                projectId: projectId
            });
            res.cookie('token', firebaseAccount, {
                maxAge: 3600000,
                secure: false,
                httpOnly: false,
            });
            res.redirect(const_1.FRONTEND_URL + "/" + "?token=" + firebaseAccount);
            // // We have an Mailchimp access token and the user identity now.
            // const accessToken = results.access_token;
            // const mailchimpUserID = results.user.id;
            // const profilePic = results.user.profile_picture;
            // const userName = results.user.full_name;
            //
            // // Create a Firebase account and get the Custom Auth Token.
        }
        catch (error) {
            console.log("Failed to generate token", error);
            // return res.jsonp({
            //   error: error.toString,
            // });
            res.redirect(const_1.FRONTEND_URL + "/" + "?error=" + error);
        }
    }));
});
/**
 * Creates a Firebase account with the given user profile and returns a custom auth token allowing
 * signing-in this account.
 * Also saves the accessToken to the datastore at /mailchimpAccessToken/$uid
 *
 * @returns {Promise<string>} The Firebase custom auth token in a promise.
 */
function createFirebaseAccount(email, displayName, mailChimpUserId, accessToken, mailchimpApiData) {
    // The UID we'll assign to the user.
    const uid = "mailchimp:" + email.replace(/[\.@]/g, '');
    // Save the access token to the Firebase Realtime Database.
    const databaseTask = admin.database().ref(`/mailAccessToken/${uid}`)
        .set(accessToken);
    // Save the access token to the Firebase Realtime Database.
    const apiDataTask = admin.database().ref(`/mailchimpApiData/${uid}`)
        .set(mailchimpApiData);
    // Create or update the user account.
    const userCreationTask = admin.auth().updateUser(uid, {
        displayName: displayName,
        email: email,
        mailChimpUserId: mailChimpUserId,
    }).catch((error) => {
        console.log("failed to update existing user by uid", uid, email, error);
        // If user does not exists we create it.
        if (error.code === 'auth/user-not-found') {
            return admin.auth().createUser({
                uid: uid,
                displayName: displayName,
                email: email,
                mailChimpUserId: mailChimpUserId,
            });
        }
        throw error;
    });
    // Wait for all async task to complete then generate and return a custom auth token.
    return Promise.all([userCreationTask, databaseTask, apiDataTask]).then(() => {
        // Create a Firebase custom auth token.
        return admin.auth().createCustomToken(uid);
    }).then((token) => {
        return token;
    });
}
//# sourceMappingURL=newToken.js.map