"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// export const OAUTH_REDIRECT_URI = `https://${process.env.GCLOUD_PROJECT}.firebaseapp.com/popup`;
// export const OAUTH_REDIRECT_URI = `http://127.0.0.1:5001/gdpr-toolkit/us-central1/token`;
exports.OAUTH_REDIRECT_URI = `https://us-central1-${process.env.GCLOUD_PROJECT}.cloudfunctions.net/token`;
exports.OAUTH_SCOPES = '';
exports.FRONTEND_URL = "https://gdpr-toolkit.firebaseapp.com";
// export const FRONTEND_URL = "http://localhost:8081/";
//# sourceMappingURL=const.js.map