"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("./const");
const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');
exports.handleLogout = functions.https.onRequest((req, res) => {
    cookieParser()(req, res, () => {
        const token = req.cookies.token;
        res.cookie('token', null, {
            maxAge: 1,
            secure: false,
            httpOnly: true,
        });
        res.redirect(const_1.FRONTEND_URL);
    });
});
//# sourceMappingURL=logout.js.map