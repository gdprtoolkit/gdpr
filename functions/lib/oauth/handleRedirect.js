"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("./client");
const const_1 = require("./const");
const crypto = require('crypto');
const functions = require('firebase-functions');
const cookieParser = require('cookie-parser');
exports.handleRedirect = functions.https.onRequest((req, res) => {
    const oauth2 = client_1.mailchimpOAuth2Client();
    cookieParser()(req, res, () => {
        const state = req.cookies.state || crypto.randomBytes(20).toString('hex');
        console.log('Setting verification state:', state);
        res.cookie('state', state.toString(), {
            maxAge: 3600000,
            secure: false,
            httpOnly: true,
        });
        const redirectUri = oauth2.authorizationCode.authorizeURL({
            redirect_uri: const_1.OAUTH_REDIRECT_URI,
            scope: const_1.OAUTH_SCOPES,
            state: state,
        });
        console.log('Redirecting to:', redirectUri);
        res.redirect(redirectUri);
    });
});
//# sourceMappingURL=handleRedirect.js.map