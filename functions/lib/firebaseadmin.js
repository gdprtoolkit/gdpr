"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebaseAdmin = require('firebase-admin');
const serviceAccount = {
    "type": "service_account",
    "project_id": "gdpr-toolkit",
    "private_key_id": "1d877b3e4c5b2a46f327c6919a5479cf6102a2fe",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDW4/1rN7sprXdJ\nMbNe2gqYXCndN1nId1C662qvp7tLfsVd7jO4SGxWs+8dLU8NjuMw7UdG6567bRWO\nEWlWRndc7GFyvFOMLHbn2OZkO9gc4xbg1UOH23i4g250rGynR909xDlCLhQHTfGs\nUTcH0KQo0xDBWyxiSi3ANjXsXL9c9HqHhcCybC4YDTFo5YCd49QqfnFnEdEvlIeP\n6YQWnZ64Vy5Oq6M02Rcq/K7thiInauypfytq720J6A6Bp0UteUTjJOq09cKZvREb\nMGkQIcNBB4zhUe9JaBqW6Eh2Nh9xAv+oT0F0VkxgBGydY95wsQWHCHmIw4DI12Qi\nZ9+t2j8bAgMBAAECggEAAKyjJ/Sl0WH/pNV+GEcbj2eCo3TAAwwKJHwGpVnOZv8/\n2U/yY5qpPwsTg1uYCuqENgFt9CqwddVyzYj/yUsxUV5V3p6kAyWZ6Jev/r0yrBcz\nrthHJ6RTAv6OmIPOAicMlQa080s3uYokvwNbkS+2GYFdZRicfAvQNvE0nB2dEB/x\neoa82bamB5uIVx8yanOyjENx34ICMsTSb4mnJt6H7Pg9Ed4Z9QlGZFAKqATCrhXo\n5QxvgdmUpJbechUzs58+SzcGW7Loh6ynu5aqKDinOAQ4dqCZVP3ONpP7uXHeXgnN\nLIk9t1NMXl2XxA1VaiKhbDEY3SVSBpSwMvrU1A3EKQKBgQD2J7Ae6e7KeMwdt2Bz\nLfR8RBly7EfeVzg89gij/P3fOlMUz3uQg5RXIUDE8IBQ0QiXgpn1Lqenuxqf297I\nMm8VbNvh/a/q9w0Tv+BzDAbM4kkYJ8B68twziHOjfHQTyvQdlwTHDwQi5VQzKmVv\nh3qXPzL3a8CsgiVl6tpk9u0QpQKBgQDffDGwKIr8Z8k7ikOoOJxcgz1oV0D6lzG3\nRl761Y79739Ha3lQwf2utcwiV16Rof5dFk1+ExhFt/e1qEveSfD7BxuCoPNntsmf\nSI/CS0qLUb/u5eLT3M5y0aJStI8+zG9LCOQYmf8lVWT2Qr2Kio6G+0IlSS1aABn+\noVEkJCNEvwKBgAq+7sxasAb8wQRnFfnRPsQSAP5oXDbTnevjXbsy9y5AkJHvANjt\nNZOcKwSt7xwYFATxOaFfeQg0fdQOuCoLMZhA2VSDS6O5R0Pec4DyPHXDRyebzKZR\nZdWGFm5Dp5mZmZCVICtEd4zCcCuPpT/6LRrfMm1ksdOWFuXdG82ejfjVAoGAHDme\nOOZV71zYQGnM87D6poB4v/XNIhj4UXJUyo8wOQgHZii/sKBffrI9LtUj/3abHnyT\nmumkdiGu6kVwF4n1Im8aXVwggafHPRO3/EzwBDp16CEmjaVkeYHaY2GkecTuBWcB\nD85bOAbZ8nQpZhbru0Z+ayEb1jMPFWagTdU2kxcCgYATHz4hOcGWakHeLv5xszwI\nW+jHwmhReCvw8DWgWwoTAqtpQ476qjeeS2DWq84ustX3ofYQq5e12ur9e7Y+tja3\nPSBbf1YBgh9UbEW3LIcq0HK671j8Ec0oT6RzH+ipCiWCSlVCN1ZEQOFhCWx/VJ0a\nK+vzOOB73KBlFEI6fb9iIw==\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-kw3by@gdpr-toolkit.iam.gserviceaccount.com",
    "client_id": "101642364256342826296",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-kw3by%40gdpr-toolkit.iam.gserviceaccount.com"
};
const firebaseApp = firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: `https://${process.env.GCLOUD_PROJECT}.firebaseio.com`,
}, "gdpr-watch");
exports.default = firebaseApp;
//# sourceMappingURL=firebaseadmin.js.map