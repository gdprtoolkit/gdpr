var firebase = require("firebase");
export function getFirebaseProjectUrl() {
    return "https://us-central1-" + firebase.app().options.authDomain.split(".")[0] + ".cloudfunctions.net";
}
