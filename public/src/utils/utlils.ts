const firebase = require("firebase");

export function getFirebaseProjectUrl() {
  // return "http://localhost:5001/gdpr-toolkit/us-central1";
  return "https://us-central1-" + firebase.app().options.authDomain.split(".")[0] + ".cloudfunctions.net";
}


