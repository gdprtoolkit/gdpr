// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import Layout from './Layout.vue'
import router from './router/router'
import store from './store/store'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import '@fortawesome/fontawesome'

Vue.use(Vuetify)


const firebase = require('firebase')


const config = {
  apiKey: "AIzaSyA56dsUVj-BZVqtnlaN5yQXX8AXN0PxLFU",
  authDomain: "gdpr-toolkit.firebaseapp.com",
  databaseURL: "https://gdpr-toolkit.firebaseio.com",
  projectId: "gdpr-toolkit",
  storageBucket: "gdpr-toolkit.appspot.com",
  messagingSenderId: "670555271976"
};

var firebaseapp = firebase.initializeApp(config);

Vue.config.productionTip = false
Vue.component("Layout", Layout);
Vue.component("loading", Layout);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router, store,
  template: '<App/>',
  components: {
    App: App,
    Layout: Layout
  }
})

