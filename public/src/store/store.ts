import Vue from 'vue'
import Vuex, {ActionTree, MutationTree, ActionContext} from 'vuex'
import * as T from '../types/common'

Vue.use(Vuex);


interface State {
  links: T.Link[],
  loggedIn: boolean,
  idToken: string,
}

const mutations: MutationTree<State> = {
  reverse: (state) => state.links.reverse(),
  loggedIn: (state) => {
    state.loggedIn = true;
  },
  setIdToken: (state: State, idToken: string) => {
    console.log("commit id token")
    state.idToken = idToken;
    state.loggedIn = true;
  }
}

const actions: ActionTree<State, any> = {
  setIdToken(store: ActionContext<State, any>, setIdToken: string) {
    console.log("set id token action")
    store.commit('setIdToken', setIdToken)
  },
}

const state: State = {
  links: [
    {url: "https://vuejs.org", description: "Core Docs"},
  ] as T.Link[],
  loggedIn: false,
  idToken: null,
}

export default new Vuex.Store<State>({
  state,
  mutations,
  actions
});



