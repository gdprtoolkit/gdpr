import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
var mutations = {
    reverse: function (state) { return state.links.reverse(); },
    loggedIn: function (state) {
        state.loggedIn = true;
    },
    setIdToken: function (state, idToken) {
        console.log("commit id token");
        state.idToken = idToken;
        state.loggedIn = true;
    }
};
var actions = {
    setIdToken: function (store, setIdToken) {
        console.log("set id token action");
        store.commit('setIdToken', setIdToken);
    },
};
var state = {
    links: [
        { url: "https://vuejs.org", description: "Core Docs" },
    ],
    loggedIn: false,
    idToken: null,
};
export default new Vuex.Store({
    state: state,
    mutations: mutations,
    actions: actions
});
