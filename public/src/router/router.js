import Vue from 'vue';
import Router from 'vue-router';
import cHome from '../components/cHome.vue';
import cAuth from '../components/cAuth.vue';
import cPopup from '../components/cPopup.vue';
import cMailingLists from "../components/cMailingLists.vue";
import cListMembers from "../components/cListMembers.vue";
Vue.use(Router);
//
// const helloRoute: RouteConfig =
//   {
//     path: '/',
//     name: 'Hello',
//     component: cHello
//   }
var routerOptions = {
    routes: [
        {
            path: '/',
            name: 'Home',
            component: cHome
        },
        {
            path: '/auth',
            name: 'Auth',
            component: cAuth
        },
        {
            path: '/popup',
            name: 'Popup',
            component: cPopup
        },
        {
            path: '/lists',
            name: "MailingLists",
            component: cMailingLists,
        },
        {
            path: '/list/:listId',
            name: "ListMembers",
            component: cListMembers,
        }
    ],
    mode: "history"
};
export default new Router(routerOptions);
