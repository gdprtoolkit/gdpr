import Vue from 'vue'
import Router, {RouterOptions} from 'vue-router'
import cHome from '../components/cHome.vue'
import cAuth from '../components/cAuth.vue'
import cMailingLists from "../components/cMailingLists.vue"
import cListMembers from "../components/cListMembers.vue"

Vue.use(Router);
//
// const helloRoute: RouteConfig =
//   {
//     path: '/',
//     name: 'Hello',
//     component: cHello
//   }

const routerOptions: RouterOptions = {
  routes: [
    {
      path: '/',
      name: 'Home',
      component: cHome
    },
    {
      path: '/lists',
      name: "MailingLists",
      component: cMailingLists,
    },
    {
      path: '/list/:listId',
      name: "ListMembers",
      component: cListMembers,
    }
  ],
  mode: "history"
}

export default new Router(
  routerOptions
)
